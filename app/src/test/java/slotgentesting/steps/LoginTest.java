package slotgentesting.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;
import slotgentesting.dto.UserData;
import slotgentesting.actions.HomePageAction;
import slotgentesting.actions.LoginPageAction;
import slotgentesting.factories.ActionsFactory;
import slotgentesting.utils.UserProvider;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:scenarios/smoke.feature"}
)
public class LoginTest extends BaseTest{
    private final LoginPageAction loginPageAction = ActionsFactory.getInstance().getAction(LoginPageAction.class);
    private final HomePageAction homePageAction = ActionsFactory.getInstance().getAction(HomePageAction.class);
    private UserData user;

    @Given("^The login page opened \"([^\"]*)\"$")
    public void openTheHomePage(String url) {
    }

    @When("The valid username: {string} entered into username field")
    public void iEnterValidIntoUsernameField(String userId) {
        user = UserProvider.INSTANCE.getById(userId);
        loginPageAction
                .setUsername(user.getUsername());
    }

    @And("The valid password for the user is entered into password field")
    public void iEnterValidIntoPasswordField() {
        String password = user.getPassword();
        loginPageAction
                .setPassword(password);
    }

    @And("The Login Button clicked")
    public void clickToTheButton() {
        loginPageAction.clickSignInButton();
    }

    @Then("Home page should be opened")
    public void homePageShouldBeOpened() {
        homePageAction
                .assertThat()
                .isPageOpened();
    }

    @Given("Open the login page")
    public void openTheLoginPage() {
        loginPageAction.openLoginPage()
                .assertThat()
                .isPageOpened();
    }

}
