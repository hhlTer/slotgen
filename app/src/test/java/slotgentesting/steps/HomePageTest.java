package slotgentesting.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;
import slotgentesting.dto.UserData;
import slotgentesting.actions.HomePageAction;
import slotgentesting.factories.ActionsFactory;
import slotgentesting.pageelements.blocks.leftbar.SubmenuItemInterface;
import slotgentesting.pages.PlayersAdminPage;
import slotgentesting.utils.CustomEnumUtils;
import slotgentesting.utils.UserProvider;

public class HomePageTest extends BaseTest{
    private HomePageAction homePageAction = ActionsFactory.getInstance().getAction(HomePageAction.class);
    @And("The valid user: {string} is logged in")
    public void theValidUserIsLoggedIn(String user) {
        UserData userData = UserProvider.INSTANCE.getById(user);
        homePageAction.assertThat()
                .isUserLogin(userData.getUsername());
    }

    @When("User opened {string} tab form the {string} menu item")
    public void userOpenedTabFormTheMenuItem(String submenuValue, String menuItemValue) {
        SubmenuItemInterface subMenu = CustomEnumUtils.extractSubMenu(menuItemValue, submenuValue);
        homePageAction
                .getHomePage()
                .getLeftBar()
                .selectMenuItem(subMenu);
    }

}
