package slotgentesting.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import slotgentesting.actions.PlayersPageAction;
import slotgentesting.factories.ActionsFactory;
import slotgentesting.pages.PlayersAdminPage;
import slotgentesting.utils.CustomEnumUtils;

public class PlayerPageTest extends BaseTest{

    private PlayersPageAction playersPageAction = ActionsFactory.getInstance().getAction(PlayersPageAction.class);

    @Then("The [User Player] page opened")
    public void theUserPlayerPageOpened() {
        playersPageAction
                .assertThat()
                .isPageOpened();
    }


    @And("The table sorted correctly by {string} column")
    public void theTableSortedCorrectlyByColumn(String columnName) {
        PlayersAdminPage.TableColumn column = CustomEnumUtils.getEnumByConstructorValue(
                PlayersAdminPage.TableColumn.class, PlayersAdminPage.TableColumn::getColumnName, columnName);
        playersPageAction
                .sortTable(column)
                .assertThat()
                .isTableSorted(column);
    }
}
