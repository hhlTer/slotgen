package slotgentesting.exceptions;

import org.openqa.selenium.ElementNotInteractableException;

/**
 * Thrown to indicate that although an element is present on the DOM, it is not visible, and so is
 * not able to be interacted with.
 */
public class ElementNotClickableException extends ElementNotInteractableException {
    public ElementNotClickableException(String message) {
        super(message);
    }
}
