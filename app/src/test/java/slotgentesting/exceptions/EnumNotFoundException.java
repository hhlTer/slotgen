package slotgentesting.exceptions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EnumNotFoundException extends RuntimeException {

    public EnumNotFoundException(List<String> list, String actualValue, Class clazz){
        final String ERROR_MESSAGE = String.format("Value do not found in the enum (%s). Enum expected values: [%s], but actual: %s",
                clazz.getName(),
                list.stream().collect(Collectors.joining(", ")),
                actualValue);
        throw new EnumNotFoundException(ERROR_MESSAGE);
    }

    private EnumNotFoundException(String errorMessage){
        super(errorMessage);
    }

    public <T extends Enum> EnumNotFoundException(Class<T> clazz, String value) {
        Enum[] enums = clazz.getEnumConstants();
        String errorMessage = String.format(
                "Value isn't found for %s. Expected value: [%s], actual values: [%s]",
                clazz.getName(),
                value,
                Arrays.stream(enums).map(Enum::name).collect(Collectors.joining(", ", "[", "]"))
        );
        throw new EnumNotFoundException(errorMessage);
    }

    public <T extends Enum> EnumNotFoundException(Class<T> clazz, int order) {
        Enum[] enums = clazz.getEnumConstants();
        String errorMessage = String.format(
                "The order isn't found for %s. Actual count of values in the enum: %d, actual values: %s, expected order: %d",
                clazz.getName(),
                enums.length,//todo:test
                Arrays.stream(enums).map(Enum::name).collect(Collectors.joining(", ", "[", "]")),
                order
        );
        throw new EnumNotFoundException(errorMessage);
    }
}