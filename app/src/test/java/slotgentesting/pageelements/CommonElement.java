package slotgentesting.pageelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class CommonElement extends BaseElement{

    public CommonElement(WebElement webelement, By by, String name) {
        super(webelement, by, name);
    }
}
