package slotgentesting.pageelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Button extends BaseElement{
    public Button(WebElement webelement, By by, String name) {
        super(webelement, by, name);
    }
    @Override
    public Button clickElement() {
        super.clickElement();
        return this;
    }
}
