package slotgentesting.pageelements.blocks;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import slotgentesting.configuration.DriverHelper;
import slotgentesting.pageelements.BaseElement;

public class BaseBlock extends BaseElement {
    public BaseBlock(WebElement webelement, By by, String name) {
        super(webelement, by, name);
    }
}
