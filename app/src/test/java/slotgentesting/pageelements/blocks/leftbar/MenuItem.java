package slotgentesting.pageelements.blocks.leftbar;

public enum MenuItem {
    DASHBOARD("s-menu-halls", Dashboard.class),
    USERS("s-menu-users", Users.class);

    private Class<? extends SubmenuItemInterface> subMenu;
    private String id;
    MenuItem(String id, Class<? extends SubmenuItemInterface> submenuItemInterfaceClass) {
        this.id = id;
        this.subMenu = submenuItemInterfaceClass;
    }

    public Class<? extends SubmenuItemInterface> getSubMenu() {
        return subMenu;
    }

    public String getId() {
        return id;
    }
}


