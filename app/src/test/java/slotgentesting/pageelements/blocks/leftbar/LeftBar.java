package slotgentesting.pageelements.blocks.leftbar;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import slotgentesting.annotations.Block;
import slotgentesting.configuration.DriverProvider;
import slotgentesting.factories.PageFactory;
import slotgentesting.pageelements.blocks.BaseBlock;
import slotgentesting.pages.BasePage;

import java.util.Arrays;

@Block
public class LeftBar extends BaseBlock {
    public LeftBar(WebElement webelement, By by, String name) {
        super(webelement, by, name);
    }

    public LeftBar expandMenu(MenuItem menuItem){
        By by = By.xpath(".//*[@id='" + menuItem.getId() + "']/..");
        driverHelper.waitAndClickWebElement(this.wrappedElement, by);
        return this;
    }

    public <T extends BasePage> T selectMenuItem(SubmenuItemInterface subMenu){
        MenuItem menuItem = Arrays.stream(MenuItem.values())
                        .filter(p -> p.getSubMenu().equals(subMenu.getClass()))
                                .findFirst().get();
        expandMenu(menuItem);
        By by = By.xpath(subMenu.getPath());
        driverHelper.waitAndClickWebElement(this.wrappedElement, by);
        return PageFactory.getInstance().getPage(subMenu.getPage());
    }
}
