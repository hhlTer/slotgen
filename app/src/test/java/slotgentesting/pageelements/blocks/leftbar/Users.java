package slotgentesting.pageelements.blocks.leftbar;

import slotgentesting.annotations.Property;
import slotgentesting.configuration.PropertiesProvider;
import slotgentesting.pages.BasePage;
import slotgentesting.pages.PlayersAdminPage;

/**
 * Data received with reflection using. See {@link slotgentesting.utils.CustomEnumUtils#extractSubMenu(String, String)}
 */
public enum Users implements SubmenuItemInterface{

    PLAYER(PropertiesProvider.propertiesHub.get(USER_PLAYER_PROPERTY_NAME), PlayersAdminPage.class);
    private Class<? extends BasePage> page;
    private String path;

    Users(String path, Class<? extends BasePage> page) {
        this.path = path;
        this.page = page;
    }

    public String getNameInLowercase() {
        return this.name().toLowerCase();
    }
    public String getPath() {
        if (path == null) {
            throw new RuntimeException("The path can't be null. Possible property doesn't set.");
        }
        return ".//a[@href='" + path + "']";
    }

    public <T extends BasePage> Class<T> getPage(){
        return (Class<T>) page;
    }
}

