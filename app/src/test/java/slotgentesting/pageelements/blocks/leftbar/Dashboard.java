package slotgentesting.pageelements.blocks.leftbar;

import slotgentesting.pages.BasePage;
import slotgentesting.pages.HomePage;

public enum Dashboard implements SubmenuItemInterface{
    DASHBOARD("", HomePage.class);

    private String path;
    private Class<? extends BasePage> page;

    Dashboard(String path, Class<? extends BasePage> page) {
        this.path = path;
    }

    @Override
    public String getPath() {
        return path;
    }

    @Override
    public String getNameInLowercase() {
        return this.name().toLowerCase();
    }


    public <T extends BasePage> Class<T> getPage(){
        return (Class<T>) page;
    }
}
