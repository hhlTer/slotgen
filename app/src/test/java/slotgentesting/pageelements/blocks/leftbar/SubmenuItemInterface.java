package slotgentesting.pageelements.blocks.leftbar;

import slotgentesting.pages.BasePage;

public interface SubmenuItemInterface {
    String getPath();
    String getNameInLowercase();
    <T extends BasePage> Class<T> getPage();
    String USER_PLAYER_PROPERTY_NAME = "user_player_admin";
}
