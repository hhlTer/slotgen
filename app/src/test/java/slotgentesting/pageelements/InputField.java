package slotgentesting.pageelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class InputField extends BaseElement{
    public InputField(WebElement webelement, By by, String name) {
        super(webelement, by, name);
    }

    public InputField setTextToField(String text){
        driverHelper.clearInputFieldAndSendText(this, text);
        return this;
    }

}
