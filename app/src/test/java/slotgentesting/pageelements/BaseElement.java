package slotgentesting.pageelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import slotgentesting.configuration.DriverHelper;

public abstract class BaseElement {
    public BaseElement(WebElement webelement, By by, String name){
        this.path = by;
        this.wrappedElement = webelement;
        this.name = name;
    }
    protected By path;
    protected WebElement wrappedElement;
    protected String name;
    protected DriverHelper driverHelper = DriverHelper.getDefaultHelper();

    protected By getPath(){
        return path;
    }

    public WebElement getWrappedElement() {
        return wrappedElement;
    }

    public String getName() {
        return name;
    }
    public boolean isDisplayed() {
        return driverHelper.isElementDisplayed(this);
    }

    public BaseElement clickElement() {
        driverHelper.waitAndClickWebElement(wrappedElement);
        return this;
    }

    public String getText(){
        return driverHelper.getText(this);
    }

    @Override
    public String toString() {
        return name;
    }
}
