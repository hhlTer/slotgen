package slotgentesting.pages;

import org.openqa.selenium.By;
import slotgentesting.annotations.Block;
import slotgentesting.pageelements.BaseElement;
import slotgentesting.pageelements.CommonElement;

@Block
public class TopBar extends BasePage{

    private final String userToggle = "//section[@id='header']//i[@class='fa fa-user']/../span";

    public BaseElement userToggle(){
        By by = By.xpath(userToggle);
        CommonElement element = elementsFactory.getElement(CommonElement.class, by, "User toggle");
        return element;
    }

    @Override
    public String getPagePath() {
        throw new RuntimeException("The Top Bar doesn't bound with the any path");
    }
}
