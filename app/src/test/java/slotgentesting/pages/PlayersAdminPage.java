package slotgentesting.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import slotgentesting.annotations.Page;
import slotgentesting.annotations.Property;

import java.util.List;

@Page
public class PlayersAdminPage extends BasePage{

    @Property(propertyName = "user_player_admin")
    private String path;
    @Override
    public String getPagePath() {
        return path;
    }

    private final String BASE_TABLE_ELEMENT_XPATH = "//section/div[@class]//div[@id='payment-system-transaction-grid']";
    private final String PATH_TO_COLUMN_INSIDE_TR = "/td[{}]";
    private final String PATH_TO_COLUMN_TOPIC_INSIDE_TABLE = "/table/thead/tr[1]/th[{}]/a";

    public WebElement getBaseTableElement(){
        By by = By.xpath(BASE_TABLE_ELEMENT_XPATH);
        return driver.findElement(by);
    }

    public List<WebElement> getRowsFromTable(){
        By by = By.xpath("./table/tbody/tr");
        return getBaseTableElement().findElements(by);
    }

    public By getPathToColumnInsideRow(TableColumn column) {
        return By.xpath("." + PATH_TO_COLUMN_INSIDE_TR.replace("{}", String.valueOf(column.columnOrder)));
    }
    public By getBaseTableByElement() {
        return By.xpath(BASE_TABLE_ELEMENT_XPATH);
    }

    public WebElement getColumnTopic(TableColumn column) {
        By by = By.xpath("." + PATH_TO_COLUMN_TOPIC_INSIDE_TABLE.replace(
                "{}",
                String.valueOf(column.columnOrder))
        );
        return getBaseTableElement().findElement(by);
    }

    public enum TableColumn {
        USERNAME(2, "Username"),
        EXTERNAL_ID(3, "External ID");

        private int columnOrder;
        private String columnName;

        TableColumn(int columnOrder, String columnName) {
            this.columnOrder = columnOrder;
            this.columnName = columnName;
        }

        public int getColumnOrder() {
            return columnOrder;
        }

        public String getColumnName() {
            return columnName;
        }
    }

}
