package slotgentesting.pages;

import org.openqa.selenium.By;
import slotgentesting.annotations.Page;
import slotgentesting.annotations.Property;
import slotgentesting.factories.ElementsFactory;
import slotgentesting.factories.PageFactory;
import slotgentesting.pageelements.blocks.leftbar.LeftBar;

@Page
public class HomePage extends BasePage{

    private TopBar topBar = PageFactory.getInstance().getPage(TopBar.class);

    @Property(propertyName = "home_page_path")
    private String path;

    @Override
    public String getPagePath() {
        return path;
    }

    public TopBar getTopBar() {
        return topBar;
    }

    public LeftBar getLeftBar() {
        By by = By.id("nav-wrapper");
        return ElementsFactory.getInstance().getElement(LeftBar.class, by, "Left Bar");
    }
}
