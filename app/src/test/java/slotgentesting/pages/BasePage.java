package slotgentesting.pages;

import org.openqa.selenium.WebDriver;
import slotgentesting.configuration.DriverProvider;
import slotgentesting.factories.ElementsFactory;
import slotgentesting.factories.PageFactory;
import slotgentesting.pageelements.BaseElement;

import java.util.HashMap;

public abstract class BasePage {

    protected ElementsFactory elementsFactory = ElementsFactory.getInstance();
    protected DriverProvider driverProvider = DriverProvider.getInstance();
    protected WebDriver driver = driverProvider.getDriver();
    protected PageFactory pageFactory = PageFactory.getInstance();

    public abstract String getPagePath();

}
