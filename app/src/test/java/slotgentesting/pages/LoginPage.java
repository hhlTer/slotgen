package slotgentesting.pages;

import org.openqa.selenium.By;
import slotgentesting.annotations.Page;
import slotgentesting.annotations.Property;
import slotgentesting.pageelements.Button;
import slotgentesting.pageelements.InputField;

@Page
public class LoginPage extends BasePage {
    private static final String signInXpath = "//input[@class='btn btn-primary btn-lg btn-block'][@type='submit']";
    private static final String usernameId = "UserLogin_username";
    private static final String passwordId = "UserLogin_password";

    @Property(propertyName = "login_page_path")
    private String loginPagePath;

    private LoginPage() {
    }

    @Override
    public String getPagePath() {
        return loginPagePath;
    }

    public Button getSignInButton(){
        By by = By.xpath(signInXpath);
        return elementsFactory.getElement(Button.class, by, "Login Button");
    }

    public InputField getUserNameInputFld(){
        By by = By.id(usernameId);
        return elementsFactory.getElement(InputField.class, by, "Username input field");
    }

    public InputField getPasswordInputFld(){
        By by = By.id(passwordId);
        return elementsFactory.getElement(InputField.class, by, "Password input field");
    }

}
