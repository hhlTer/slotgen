package slotgentesting.configuration;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import slotgentesting.exceptions.ElementNotClickableException;
import slotgentesting.factories.PageFactory;
import slotgentesting.pageelements.BaseElement;
import slotgentesting.pages.BasePage;
import slotgentesting.utils.Utils;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.IntStream.range;

public class DriverHelper implements SearchContext {

    private final static int PROPERTY_WAIT = EnvironmentProvider.TIMEOUT;
    private final static int TWO_SECONDS_WAIT = 2000;
    private final static int HALF_MINUTE_WAIT = 30000;
    private static DriverProvider driverProvider = DriverProvider.getInstance();
    private static DriverHelper DEFAULT_WAIT_TIME = new DriverHelper(PROPERTY_WAIT);
    private static DriverHelper LONG_WAIT_TIME = new DriverHelper(HALF_MINUTE_WAIT);
    private static DriverHelper SHORT_WAIT_TIME = new DriverHelper(TWO_SECONDS_WAIT);
    private WebDriverWait wait;

    public static DriverHelper getDefaultHelper() {
        return DEFAULT_WAIT_TIME;
    }

    public static DriverHelper getLongWaitHelper() {
        return LONG_WAIT_TIME;
    }

    public static DriverHelper getShortWaitHelper() {
        return SHORT_WAIT_TIME;
    }


    private DriverHelper() {
        this(EnvironmentProvider.TIMEOUT);
    }

    private DriverHelper(int timeout) {
        this.wait = new WebDriverWait(driverProvider.getDriver(), timeout);
    }


    @Override
    public List<WebElement> findElements(By by) {
        List<WebElement> result = driverProvider.getDriver().findElements(by);
        Logging.info("The number of elements is for path" + by.toString() + result);
        return result;
    }

    @Override
    public WebElement findElement(By by) {
        return driverProvider.getDriver().findElement(by);
    }

    public WebElement waitAndClickWebElement(WebElement element) {
        Logging.info("Try to click " + element);
        try {
            waitToBeClickable(element).click();
        } catch (Exception e) {
            try {
                element.click();
            } catch (Exception e1) {
                Utils.delayMillisec(2000);
                Logging.warn(element + " isn't clicked, try to click one more time");
                element.click();
            }
        }
        Logging.info("Element " + element + " is clicked.");
        return element;
    }

    public WebElement waitAndClickWebElement(WebElement parent, By by) {
        waitToBeClickable(parent);

        try {
            WebElement element = parent.findElement(by);
            return waitAndClickWebElement(element);
        } catch (Exception e) {
            Logging.error("The element isn't found on the " + parent +
                    "#" + by.toString());
            throw new RuntimeException(e);
        }
    }

    public void waitForPageLoad(int timeInMilliseconds) {
        WebDriverWait w = new WebDriverWait(driverProvider.getDriver(), timeInMilliseconds);
        Function<WebDriver, Boolean> f = driver -> String
                .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                .equals("complete");
        try {
            w.until(f);
        } catch (Throwable e) {
            ((JavascriptExecutor) driverProvider.getDriver()).executeScript("window.stop();");
            w.until(f);
        }
        w.until(driver -> String
                .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                .equals("complete"));
        Logging.info("Waiting via JS for the page loading");
    }

    public WebElement waitToBeClickable(WebElement element) {
        try {
            return wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {
            String message = String.format("The element '%s' isn't found. \n %s",
                    element.toString(),
                    Optional.of(e.getMessage()).orElse("No exception message"));
            Logging.info(message);
            throw new ElementNotClickableException(message);
        }
    }

    public <T extends BasePage> T openPage(Class<T> pageClass) {
        T t = PageFactory.getInstance().getPage(pageClass);
        String baseUrl = EnvironmentProvider.BASE_URL;
        String path = t.getPagePath();
        String uri = normalizeUri(baseUrl, path);
        driverProvider.getDriver().get(uri);
        return t;
    }

    /**
     * Concatenate baseUrl and path in the way that exclude wrong URI without '/' symbol
     * in the case when baseUrl value doesn't end with "/" and at the same time the `path`
     * parameter doesn't start with "/". Also exclude "//" case
     *
     * @param baseUrl
     * @param path
     * @return right baseUrl + path concatenation
     */
    public String normalizeUri(String baseUrl, String path) {
        String http = baseUrl.split("://")[0];
        String url = baseUrl.split("://")[1] + "/" + path;
        while (url.contains("//"))
            url = url.replaceAll("//", "/");
        return http + "://" + url;
    }

    public boolean isElementDisplayed(BaseElement baseElement) {
        boolean result;
        try {
            result = wait.until((driver) -> baseElement.getWrappedElement().isDisplayed());
        } catch (Exception e) {
            result = false;
        }

        Logging.info("The " + baseElement + " is" + (result ? " " : "n't " + " displayed"));
        return result;
    }

    public void clearInputFieldAndSendText(BaseElement inputFld, String value) {
        WebElement inputElement = inputFld.getWrappedElement();
        String currentValue = wait.until(ExpectedConditions.visibilityOf(inputElement)).getText();
        if (currentValue.length() == 0) {
            currentValue = inputElement.getAttribute("value");
        }
        if (currentValue.length() > 0) {
            clearInputField(inputElement);
        }
        inputElement.sendKeys(value);
        Logging.info("The " + value + " is typed to " + inputFld.getName());
    }

    public WebElement clearInputField(WebElement element) {
        waitAndClickWebElement(element);
        element.clear();
        element.sendKeys(Keys.chord(Keys.CONTROL, "a", Keys.DELETE));

        range(0, 10).forEach(value -> {
            element.sendKeys(Keys.DELETE);
            element.sendKeys(Keys.BACK_SPACE);
        });
        Logging.info("The " + element + " is cleared");
        return element;
    }

    public String getText(BaseElement baseElement) {
        String result = baseElement.getWrappedElement().getText();
        Logging.info("The text of the element " + baseElement.getName() + " is accepted: " + result);
        return result;
    }

    public WebElement waitForAttributeEquals(By by, String attribute, String expectedValue, int waitingTimeInMilliseconds) {
        WebElement element = findElement(by);
        long checkpoint = System.currentTimeMillis() + waitingTimeInMilliseconds;
        String attrValue = "";
        while (System.currentTimeMillis() < checkpoint) {
            attrValue = element.getAttribute(attribute);
            if (attrValue.equals(expectedValue)) {
                Logging.info(element + " is equals to [" + expectedValue + "] in the attribute " + attribute);
                return element;
            } else {
                Utils.delayMillisec(1000);
                element = findElement(by);
            }
        }
        if (attrValue.equals(expectedValue)) {
            Logging.info(element + " is equals to [" + expectedValue + "] in the attribute " + attribute);
            return element;
        } else {
            Logging.info(element + " isn't equals to " + expectedValue + " in the attribute " + attribute);
            throw new RuntimeException(element + " still contains " + expectedValue + " in the attribute " + attribute);
        }
    }

}

