package slotgentesting.configuration;

public enum Environment {
    DEVELOPMENT(
            "http://test-app.d6.dev.devcaz.com",
            "dev"
    ),
    PRODUCTION(
            "http://test-app.d6.prod.devcaz.com",
            "prod"
    );

    private String url;
    private String environmentValue;

    Environment(String url, String environmentValue) {
        this.url = url;
        this.environmentValue = environmentValue;
    }

    public String getUrl() {
        return url;
    }

    public String getEnvironmentValue() {
        return environmentValue;
    }
}
