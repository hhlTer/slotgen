package slotgentesting.configuration;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PropertiesProvider {
    public static Map<String,String> propertiesHub;

    static {
        String path = PropertiesProvider.class.getClassLoader().getResource("application.properties").getPath();
        assert path != null;
        Stream<String> lines = null;
        try {
            lines = new BufferedReader(new FileReader(path)).lines();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        propertiesHub = lines
                .map(line -> line.split("="))
                .collect(Collectors.toMap(arr -> arr[0].trim(), arr -> arr[1].trim()));
    }
}
