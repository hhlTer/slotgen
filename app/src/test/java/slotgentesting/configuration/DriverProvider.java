package slotgentesting.configuration;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import static slotgentesting.configuration.EnvironmentProvider.*;

public class DriverProvider {
    private static final DriverProvider INSTANCE = new DriverProvider();
    public static DriverProvider getInstance(){
        return INSTANCE;
    }
    private static final ThreadLocal<WebDriver> driverPool = new ThreadLocal<>();
    private final static Set<WebDriver> quitSet = new HashSet<>();

    private DriverProvider(){
        Runtime.getRuntime().addShutdownHook(
                new Thread(() ->
                        quitSet.forEach(WebDriver::quit)
                ));
    }

    public void initDriver(){
        WebDriver driver;
        if (BROWSER == Browser.FIREFOX) {
            driver = initFirefox();
        } else {
            driver = initChromeDriver();
        }
        driver.manage().window().maximize();
        driverPool.set(driver);
        quitSet.add(driver);
    }

    private WebDriver initChromeDriver() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        return driver;
    }
    private WebDriver initFirefox() {
        WebDriverManager.firefoxdriver().setup();;
        WebDriver driver = new FirefoxDriver();
        return driver;
    }

    public WebDriver getDriver(){
        if(driverPool.get() == null){
            initDriver();
        }
        return driverPool.get();
    }

}
