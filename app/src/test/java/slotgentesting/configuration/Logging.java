package slotgentesting.configuration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Logging {

    static {
        String pathToFile = System.getProperty("user.home") +
                System.getProperty("file.separator") +
                "SlotGen.log";
        System.setProperty("log.filename", pathToFile);
    }

    //Info Level Logs
    public static void info (String message) {
        log(LogLevel.INFO, message);
    }

    //Warn Level Logs
    public static void warn (String message) {
        log(LogLevel.WARNING, message);
    }

    //Error Level Logs
    public static void error (String message) {
        log(LogLevel.ERROR, message);
        StackTraceElement element = Thread.currentThread().getStackTrace()[2];
        log(LogLevel.ERROR, "Class: " + element.getClassName());
        log(LogLevel.ERROR, "Method: " + element.getMethodName());
        log(LogLevel.ERROR, "Line error: " + element.getLineNumber());
    }

    //Debug Level Logs
    public static void debug (String message) {
        log(LogLevel.DEBUG, message);
    }

    private static void log(LogLevel logLevel, String message){
        String result = "[" +
                logLevel.name().toUpperCase() + "] " +
                getCurrentDate() + " " +
                Thread.currentThread().getName() + " | " +
                message;
        System.out.println(result);
    }

    private static String getCurrentDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM.dd HH:mm:ss", Locale.ENGLISH);
        return dateFormat.format(new Date());
    }


    enum LogLevel{
        INFO,
        WARNING,
        ERROR,
        DEBUG
    }
}
