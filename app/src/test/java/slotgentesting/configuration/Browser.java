package slotgentesting.configuration;

public enum Browser {
    CHROME("chrome"),
    FIREFOX("firefox");

    private String browserName;

    Browser(String browserName) {
        this.browserName = browserName;
    }

    public String getBrowserName() {
        return browserName;
    }
}
