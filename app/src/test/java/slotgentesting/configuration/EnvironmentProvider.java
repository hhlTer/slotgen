package slotgentesting.configuration;

import slotgentesting.utils.CustomEnumUtils;

import static slotgentesting.configuration.EnvironmentProvider.InitializeProperties.*;

public class EnvironmentProvider {
    public static Environment ENVIRONMENT = ENVIRONMENT_VALUE == null ? DEFAULT_ENVIRONMENT :
            CustomEnumUtils.getEnumByConstructorValue(
                    Environment.class, Environment::getEnvironmentValue, ENVIRONMENT_VALUE);
    public static String BASE_URL = ENVIRONMENT.getUrl();

    public static Browser BROWSER = BROWSER_VALUE == null ? DEFAULT_BROWSER :
            CustomEnumUtils.getEnumByConstructorValue(Browser.class, Browser::getBrowserName, BROWSER_VALUE);
    public static int TIMEOUT = Integer.parseInt(TIMEOUT_VALUE);

    static class InitializeProperties {
        final static String ENVIRONMENT_PROPERTY = "environment";
        final static String TIMEOUT_PROPERTY = "wait";
        final static String BROWSER_PROPERTY = "browser";

        final static int DEFAULT_TIMEOUT = 5000; //15 seconds
        final static Browser DEFAULT_BROWSER = Browser.CHROME;
        final static Environment DEFAULT_ENVIRONMENT = Environment.DEVELOPMENT;
        final static String ENVIRONMENT_VALUE;
        final static String BROWSER_VALUE;

        static {
            String systemEnvironmentProperty = System.getProperty(ENVIRONMENT_PROPERTY);
            if (systemEnvironmentProperty != null && systemEnvironmentProperty.trim().length() > 0) {
                ENVIRONMENT_VALUE = systemEnvironmentProperty;
            } else {
                ENVIRONMENT_VALUE = PropertiesProvider.propertiesHub.get(ENVIRONMENT_PROPERTY);
            }
        }

        static String TIMEOUT_VALUE =
                PropertiesProvider.propertiesHub.containsKey(TIMEOUT_PROPERTY) &&
                        PropertiesProvider.propertiesHub.get(TIMEOUT_PROPERTY) != null ?
                        PropertiesProvider.propertiesHub.get("wait") : String.valueOf(DEFAULT_TIMEOUT);

        static {
            try {
                Integer.parseInt(TIMEOUT_VALUE);
            } catch (NumberFormatException e) {
                TIMEOUT_VALUE = String.valueOf(DEFAULT_TIMEOUT);
            }
        }

        static {
            String env = System.getenv(BROWSER_PROPERTY);
            String systemProperty = System.getProperty(BROWSER_PROPERTY);
            String applicationProperty = PropertiesProvider.propertiesHub.get(BROWSER_PROPERTY);
            BROWSER_VALUE = env != null ? env :
                    systemProperty != null ? systemProperty :
                            applicationProperty;
        }

    }
}
