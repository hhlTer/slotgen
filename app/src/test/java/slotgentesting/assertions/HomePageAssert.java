package slotgentesting.assertions;

import org.junit.Assert;
import slotgentesting.actions.HomePageAction;
import slotgentesting.configuration.DriverProvider;
import slotgentesting.factories.ActionsFactory;

public class HomePageAssert extends BaseAssert{

    private final HomePageAction HOME_PAGE_ACTION = ActionsFactory.getInstance().getAction(HomePageAction.class);

    public HomePageAssert isPageOpened() {

        final String EXPECTED_PATH = HOME_PAGE_ACTION.getHomePage().getPagePath();
        final String ACTUAL_URI = DriverProvider.getInstance().getDriver().getCurrentUrl();
        boolean isUserToggleAppeared;
        try{
            isUserToggleAppeared = HOME_PAGE_ACTION.getHomePage().getTopBar().userToggle().isDisplayed();
        } catch (Exception e){
            isUserToggleAppeared = false;
        }
        Assert.assertTrue(
                "Expected the Top Bar menu is opened but isn't",
                isUserToggleAppeared
                );

        Assert.assertTrue(
                "Expected path is: " + HOME_PAGE_ACTION.getHomePage().getPagePath() +
                        " but full uri is: " + ACTUAL_URI,
                ACTUAL_URI.endsWith(EXPECTED_PATH)
        );
        return this;
    }

    public HomePageAssert isUserLogin(String expectedUsername){
        String actualUserName =
                HOME_PAGE_ACTION.getHomePage()
                        .getTopBar()
                        .userToggle().getText();

        boolean erq = actualUserName.equals(expectedUsername);
        Assert.assertEquals(
                "Unexpected user is logged in to the system. Expected: [" + expectedUsername +
                        "] Actual: [" + actualUserName + "]",
                expectedUsername,
                actualUserName
                );
        return this;
    }

    public HomePageAssert and() {
        return this;
    }
}
