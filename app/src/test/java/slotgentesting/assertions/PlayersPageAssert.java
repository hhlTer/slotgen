package slotgentesting.assertions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import slotgentesting.actions.PlayersPageAction;
import slotgentesting.configuration.DriverProvider;
import slotgentesting.configuration.Logging;
import slotgentesting.factories.ActionsFactory;
import slotgentesting.factories.PageFactory;
import slotgentesting.pages.PlayersAdminPage;

import java.util.List;
import java.util.stream.Collectors;

public class PlayersPageAssert extends BaseAssert {

    private static final PlayersAdminPage PLAYERS_ADMIN_PAGE = PageFactory.getInstance().getPage(PlayersAdminPage.class);

    public PlayersPageAssert isPageOpened() {

        final String EXPECTED_PATH = PLAYERS_ADMIN_PAGE.getPagePath();
        final String ACTUAL_URI = DriverProvider.getInstance().getDriver().getCurrentUrl();
        boolean isTableAppeared;
        try {
            PLAYERS_ADMIN_PAGE.getBaseTableElement();
            isTableAppeared = true;
        } catch (Exception e) {
            isTableAppeared = false;
        }
        Assert.assertTrue(
                "Expected the Users Table is opened but isn't",
                isTableAppeared
        );

        Assert.assertTrue(
                "Expected path is: " + PLAYERS_ADMIN_PAGE.getPagePath() +
                        " but full uri is: " + ACTUAL_URI,
                ACTUAL_URI.endsWith(EXPECTED_PATH)
        );
        return this;
    }

    public PlayersPageAssert isTableSorted(PlayersAdminPage.TableColumn column) {
        List<WebElement> rows = PLAYERS_ADMIN_PAGE.getRowsFromTable();
        By columnBy = PLAYERS_ADMIN_PAGE.getPathToColumnInsideRow(column);
        final List<String> ACTUAL_LIST = rows.stream()
                .map(p -> p.findElement(columnBy))
                .map(WebElement::getText)
                .collect(Collectors.toList());
        final List<String> EXPECTED_SORTED_LIST = ACTUAL_LIST.stream().sorted().collect(Collectors.toList());
        boolean isListSortedASC = EXPECTED_SORTED_LIST.equals(ACTUAL_LIST);
        Logging.debug("Sorted list: " + ACTUAL_LIST);
        Assert.assertTrue(
                "The list isn't sorted correctly. Expected: " + EXPECTED_SORTED_LIST +
                        "\nActual: " + ACTUAL_LIST,
                isListSortedASC
        );
        return this;
    }
}
