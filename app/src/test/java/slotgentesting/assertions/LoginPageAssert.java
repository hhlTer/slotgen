package slotgentesting.assertions;

import org.junit.Assert;
import slotgentesting.actions.LoginPageAction;
import slotgentesting.factories.ActionsFactory;
import slotgentesting.pageelements.Button;

public class LoginPageAssert extends BaseAssert{
    private final LoginPageAction LOGIN_PAGE_ACTION = ActionsFactory.getInstance().getAction(LoginPageAction.class);
    public LoginPageAssert isPageOpened() {
        try {
            Button button = LOGIN_PAGE_ACTION.getLoginPage().getSignInButton();
            Assert.assertTrue(
                    "The element " + button + " isn't displayed. ",
                    button.isDisplayed()
            );
        } catch (Exception e){
            throw new RuntimeException("The webelement isn't displayed. Stacktrace:\n" + e.getMessage());
        }
        return this;
    }
}
