package slotgentesting.dto;

import org.jdom2.Element;

import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserData {

    private String username;
    private String password;
    private String userId;

    public UserData(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserData(Element element) {
        userId = element.getAttributeValue("userId");
        List<Element> children = element.getChildren();
        for (Element tag : children) {
            switch (tag.getName()) {
                case "username":
                    username = tag.getText();
                    break;
                case "password":
                    password = tag.getText();
                    break;
                default:
            }
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return String.format("ID: %s, Username: %s, Password: %s", userId, username, password);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserData userData = (UserData) o;
        return username.equals(userData.username) &&
                password.equals(userData.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), username, password);
    }

    public String getMailLoginWithoutAlias() {
        Matcher matcher = Pattern.compile(".(\\+[0-9a-zA-Z]+)@").matcher(username);
        return matcher.find() ? username.replace(matcher.group(1), "") : username;
    }
}
