package slotgentesting.actions;

import slotgentesting.assertions.HomePageAssert;
import slotgentesting.factories.AssertionFactory;
import slotgentesting.factories.PageFactory;
import slotgentesting.pages.HomePage;

public class HomePageAction extends BaseAction{

    private final HomePage HOME_PAGE = PageFactory.getInstance().getPage(HomePage.class);

    @Override
    public HomePageAssert assertThat() {
        return AssertionFactory.getInstance().getAssertion(HomePageAssert.class);
    }

    public HomePage getHomePage() {
        return HOME_PAGE;
    }

}
