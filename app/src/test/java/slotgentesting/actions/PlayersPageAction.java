package slotgentesting.actions;

import org.openqa.selenium.By;
import slotgentesting.assertions.PlayersPageAssert;
import slotgentesting.factories.AssertionFactory;
import slotgentesting.factories.PageFactory;
import slotgentesting.pages.PlayersAdminPage;
import slotgentesting.utils.Utils;

public class PlayersPageAction extends BaseAction{

    private final PlayersAdminPage playersAdminPage = PageFactory.getInstance().getPage(PlayersAdminPage.class);
    @Override
    public PlayersPageAssert assertThat() {
        return AssertionFactory.getInstance().getAssertion(PlayersPageAssert.class);
    }

    public PlayersPageAction sortTable(PlayersAdminPage.TableColumn column){
        driverHelper.waitAndClickWebElement(playersAdminPage.getColumnTopic(column));
        driverHelper.waitForPageLoad(2000);
        waitUntilTableRefreshed(5000);
        return this;
    }

    public PlayersPageAction waitUntilTableRefreshed(int waitingTimeInMilliseconds){
        Utils.delayMillisec(1000);
        By pathToBaseTableElement = playersAdminPage.getBaseTableByElement();
        driverHelper.waitForAttributeEquals(
                pathToBaseTableElement,
                "class",
                "grid-view",
                waitingTimeInMilliseconds);
        return this;
    }
}
