package slotgentesting.actions;

import slotgentesting.assertions.LoginPageAssert;
import slotgentesting.factories.PageFactory;
import slotgentesting.pages.LoginPage;

public class LoginPageAction extends BaseAction {
    private final LoginPage LOGIN_PAGE = PageFactory.getInstance().getPage(LoginPage.class);
    public LoginPageAction(){}
    public LoginPageAssert assertThat(){
        return new LoginPageAssert();
    }
    public LoginPageAction clickSignInButton(){
        LOGIN_PAGE
                .getSignInButton()
                .clickElement();
        driverHelper.waitForPageLoad(20_000);
        return this;
    }

    public LoginPage getLoginPage() {
        return LOGIN_PAGE;
    }

    public LoginPageAction openLoginPage(){
        driverHelper.openPage(LoginPage.class);
        return this;
    }

    public void setPassword(String password) {
        LOGIN_PAGE
                .getPasswordInputFld().setTextToField(password);
    }

    public void setUsername(String username) {
        LOGIN_PAGE
                .getUserNameInputFld().setTextToField(username);
    }

}
