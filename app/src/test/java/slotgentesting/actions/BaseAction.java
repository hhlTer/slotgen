package slotgentesting.actions;

import org.openqa.selenium.WebDriver;
import slotgentesting.assertions.BaseAssert;
import slotgentesting.configuration.DriverHelper;
import slotgentesting.configuration.DriverProvider;

public abstract class BaseAction {
    protected WebDriver driver = DriverProvider.getInstance().getDriver();
    protected DriverHelper driverHelper = DriverHelper.getDefaultHelper();
    public abstract <T extends BaseAssert> T assertThat();

}
