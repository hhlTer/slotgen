package slotgentesting.factories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import slotgentesting.annotations.Block;
import slotgentesting.annotations.Page;
import slotgentesting.configuration.DriverProvider;
import slotgentesting.pageelements.BaseElement;

import java.lang.reflect.Constructor;
import java.util.*;
import java.util.function.Supplier;

public class ElementsFactory {

    private DriverProvider driverProvider = DriverProvider.getInstance();
    private static final ElementsFactory INSTANCE = new ElementsFactory();
    private static Map<String, BaseElement> elementsStore = new HashMap<>();

    public static ElementsFactory getInstance() {
        return INSTANCE;
    }

    private ElementsFactory() {
    }

    public <T extends BaseElement> T getElement(
            final Class<T> elementClazz,
            final By by,
            final Supplier<WebElement> elementInitializer,
            final String name) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        String calledPage = Arrays.stream(elements)
                .filter(p ->
                    !p.getClassName().contains(this.getClass().getName()) &&
                    !p.getClassName().contains("Thread")
                )
                .filter( p -> {
                            try {
                                Class<?> clazz = Class.forName(p.getClassName());
                                return clazz.getAnnotation(Block.class) != null ||
                                        clazz.getAnnotation(Page.class) != null;
                            } catch (Exception e){
                                throw new RuntimeException(e);
                            }
                        }
                )
                .findFirst().get().getClassName();
        final String key = calledPage + "#" + name;
        if(!elementsStore.containsKey(key)){
            try {
                WebElement wrappedElement = elementInitializer.get();
                Constructor<T> constructor= elementClazz.getConstructor(WebElement.class, By.class, String.class);
                constructor.setAccessible(true);
                T result = constructor.newInstance(wrappedElement, by, name);
                elementsStore.put(key, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return (T) elementsStore.get(key);
    }

    public <T extends BaseElement> T getElement(Class<T> elementClazz, By by, String name) {
        Supplier<WebElement> initializer = () ->
            driverProvider.getDriver().findElement(by);

        return getElement(elementClazz, by, initializer, name);

    }
}
