package slotgentesting.factories;

import slotgentesting.annotations.Property;
import slotgentesting.configuration.PropertiesProvider;
import slotgentesting.pages.BasePage;

import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class PageFactory {
    private static final PageFactory INSTANCE = new PageFactory();
    private static final Map<Class<? extends BasePage>, BasePage> pageStore = new HashMap<>();

    public static PageFactory getInstance() {
        return INSTANCE;
    }

    private PageFactory() {
    }

    public <T extends BasePage> T getPage(Class<T> pageClazz) {
        if(!pageStore.containsKey(pageClazz)){
            try {
                Constructor<T> constructor= pageClazz.getDeclaredConstructor();
                constructor.setAccessible(true);
                T result = constructor.newInstance();
                initProperties(pageClazz, result);
                pageStore.put(pageClazz, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return (T)pageStore.get(pageClazz);
    }

    private <T extends BasePage> void initProperties(Class<T> clazz, T page) throws FileNotFoundException, IllegalAccessException {

        for (Field field :
                clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(Property.class)) {
                Property propertyAnnotation = field.getAnnotation(Property.class);
                String propertyName = propertyAnnotation.propertyName();
                String value = "";
                if (propertyName == null || propertyName.isEmpty()) {
                    propertyName = "#" + field.getName();
                } else {
                    propertyName = propertyName.trim().replaceAll("\\s+", "");
                }
                if (propertyName.startsWith("#")) {
                    propertyName = propertyName.replace("#", "");
                    value = propertyName;
                } else {
                    value = PropertiesProvider.propertiesHub.get(propertyName);
                    if(value == null || value.length() == 0){
                        throw new RuntimeException("The property do not set for the field " + field.getName()
                                + ", in the class " + clazz.getName()
                                + " please add property to the resources\\application.properties file"
                        );
                    }
                }
                field.setAccessible(true);
                field.set(page, value);
            }
        }
    }

    public <T extends BasePage> T renewAndGetPage(Class<T> pageClazz) {
        try {
            T result = (T) pageClazz.getDeclaredConstructor().newInstance();
            pageStore.put(pageClazz, result);
            return (T) pageStore.get(pageClazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
