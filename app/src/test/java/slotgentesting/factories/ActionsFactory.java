package slotgentesting.factories;


import slotgentesting.actions.BaseAction;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class ActionsFactory {

    private static final ActionsFactory INSTANCE = new ActionsFactory();
    private static Map<Class<? extends BaseAction>, BaseAction> actionStore = new HashMap<>();

    public static ActionsFactory getInstance() {
        return INSTANCE;
    }

    private ActionsFactory() {
    }

    public <T extends BaseAction> T getAction(Class<T> actionClass) {
        if(!actionStore.containsKey(actionClass)){
            try {
                Constructor<T> constructor= actionClass.getDeclaredConstructor();
                constructor.setAccessible(true);
                T result = constructor.newInstance();
                actionStore.put(actionClass, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return (T) actionStore.get(actionClass);
    }
}
