package slotgentesting.factories;

import slotgentesting.assertions.BaseAssert;
import slotgentesting.pageelements.BaseElement;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

public class AssertionFactory {

    private static final AssertionFactory INSTANCE = new AssertionFactory();
    private static Map<Class<? extends BaseAssert>, BaseAssert> assertionStore = new HashMap<>();

    public static AssertionFactory getInstance() {
        return INSTANCE;
    }

    private AssertionFactory() {
    }

    public <T extends BaseAssert> T getAssertion(Class<T> assertionClazz) {
        if(!assertionStore.containsKey(assertionClazz)){
            try {
                Constructor<T> constructor= assertionClazz.getDeclaredConstructor();
                constructor.setAccessible(true);
                T result = constructor.newInstance();
                assertionStore.put(assertionClazz, result);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return (T) assertionStore.get(assertionClazz);
    }
}
