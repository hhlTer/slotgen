package slotgentesting.utils;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import slotgentesting.dto.UserData;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

public enum UserProvider {

    INSTANCE;

    private final String USERS_DATA_FILE_NAME = "userData.xml";

    private Set<UserData> userPool;

    /**
     * Default constructor.
     * Parses an xml file and fills up the userPool field with user data.
     */

    UserProvider() throws RuntimeException {
        File usersDataFile = new File(getClass().getResource("/" + USERS_DATA_FILE_NAME).getFile());
        userPool = new HashSet<>();
        try {
            Document document = new SAXBuilder().build(usersDataFile);
            document.getRootElement().getChildren().forEach(e -> userPool.add(new UserData(e)));
        } catch (JDOMException | IOException e) {
            throw new RuntimeException("AdminConsoleUserModel XML parsing failed!", e);
        }
    }

    /**
     * Gets user by their id attribute in the XML.
     *
     * @param id {@link String} query against XML id param value.
     * @return Any matching {@link UserData} instance.
     * @throws NoSuchElementException if no items match the query.
     */

    public UserData getById(String id) throws NoSuchElementException {
        return userPool.stream().filter(data -> data.getId().equals(id)).findAny()
                .orElseThrow(NoSuchElementException::new);
    }

    public UserData getByUserName(String username) throws NoSuchElementException {
        return userPool.stream().filter(data -> data.getUsername().equals(username)).findAny()
                .orElseThrow(NoSuchElementException::new);
    }
}
