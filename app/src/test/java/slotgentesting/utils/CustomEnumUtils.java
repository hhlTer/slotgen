package slotgentesting.utils;

import org.apache.commons.lang3.EnumUtils;
import slotgentesting.exceptions.EnumNotFoundException;
import slotgentesting.pageelements.blocks.leftbar.MenuItem;
import slotgentesting.pageelements.blocks.leftbar.SubmenuItemInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class CustomEnumUtils {

    public static <T extends Enum> T getEnum(Class<T> clazz, String enumName){
        return (T) EnumUtils.getEnum(clazz, enumName.toUpperCase());
    }
    public static <T extends Enum> T getEnumByConstructorValue(
            Class<T> clazz,
            Function<T, String> getter,
            String value
            ){
        value = value.trim().toLowerCase().replaceAll("\\s+", "");
        try {
            T[] enums = clazz.getEnumConstants();
            List<String> valuesForReport = new ArrayList<>();
            for (T t :
                    enums) {
                String actualValue = getter.apply(t);
                actualValue = actualValue
                        .replaceAll("\\s+", "")
                        .toLowerCase();
                valuesForReport.add(actualValue);
                if(value.equals(actualValue)){
                    return t;
                }
            }
            throw new EnumNotFoundException(valuesForReport, value, clazz);
        } catch (RuntimeException e) {
            throw new RuntimeException(
                    String.format("Method %s do not declared in the enum %s", getter, clazz.getName())
            );
        }
    }

    public static SubmenuItemInterface extractSubMenu(Class<? extends SubmenuItemInterface> subMenu, String submenuValue) {
        return Arrays.stream(subMenu.getEnumConstants())
                .peek(p -> System.out.println(p.getNameInLowercase()))
                .filter(p -> p.getNameInLowercase().equals(submenuValue.toLowerCase()))
                .findFirst().get();
    }

    public static SubmenuItemInterface extractSubMenu(String menuItemValue, String submenuValue) {
        MenuItem menuItem = CustomEnumUtils.getEnum(MenuItem.class, menuItemValue.toUpperCase());
        Class<? extends SubmenuItemInterface> subMenuClass = menuItem.getSubMenu();
        return extractSubMenu(subMenuClass, submenuValue);
    }
}
