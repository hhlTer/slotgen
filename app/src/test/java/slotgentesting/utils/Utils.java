package slotgentesting.utils;

public class Utils {
    public static void delayMillisec(int delay) {
        long checkpoint = System.currentTimeMillis() + delay;
        while (checkpoint > System.currentTimeMillis()){
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                //ignore
            }
        }
    }
}
