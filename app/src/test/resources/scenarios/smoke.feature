Feature: Business logic test

  Scenario: The user able to login with valid credentials
    Given Open the login page
    When The valid username: "admin" entered into username field
    And The valid password for the user is entered into password field
    And The Login Button clicked
    Then Home page should be opened
    And The valid user: "admin" is logged in

  Scenario: The user able to see the table on the Players tab
    When User opened "Player" tab form the "Users" menu item
    Then The [User Player] page opened
    And The table sorted correctly by "Username" column